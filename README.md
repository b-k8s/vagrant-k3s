# Vagrant bootstrap for k3s

This is a basic setup to bootstrap 1 x k3s master node and _n_ (default _2_)
k3s worker nodes

Requires libirt, KVM, et al to be set up -- VirtualBox not (currently) supported

