#/bin/bash
set -ex

export SERVER_IP=$(ip a | grep global | grep eth1 | awk '{print $2}' | cut -f1 -d '/')

yum install -y container-selinux selinux-policy-base
rpm -i https://rpm.rancher.io/k3s-selinux-0.1.1-rc1.el7.noarch.rpm

# disabled as mongo doesn't like this
echo never >/host-sys/kernel/mm/transparent_hugepage/enabled

curl -sfL https://get.k3s.io | \
  INSTALL_K3S_SYMLINK="true" \
  INSTALL_K3S_EXEC="\
  --node-ip=${SERVER_IP} \
  --node-external-ip=${SERVER_IP} \
  --bind-address=${SERVER_IP}" \
  sh -

echo $SERVER_IP > /vagrant/server-ip
sudo cp /var/lib/rancher/k3s/server/node-token /vagrant/node-token
sudo cp /etc/rancher/k3s/k3s.yaml /vagrant/k3s.yaml
sudo sed -i -e "s/127.0.0.1/${SERVER_IP}/g" /vagrant/k3s.yaml
