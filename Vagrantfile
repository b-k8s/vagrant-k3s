# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'libvirt'
ENV['VAGRANT_NO_PARALLEL'] = 'yes'

number_of_workers = (ENV['K3S_WORKERS'] || "2").to_i
box_name = "centos/7"
box_ver = "2004.01"

Vagrant.configure("2") do |config|
  config.vm.box = "#{box_name}"
  config.vm.box_version = "#{box_ver}"

  config.vm.define "server" do |server|
    server.vm.hostname = 'server'
    server.vm.network :private_network,
      :ip => "192.168.80.10",
      :netmask => "255.255.255.0"
    server.vm.synced_folder './', '/vagrant', type: 'nfs'
    server.vm.provision :shell, :path => "server.sh"
    server.vm.provider :libvirt do |vm|
        vm.driver = 'kvm'
        vm.cpu_mode = 'host-passthrough'
        vm.cpus=1
        vm.memory=2048
    end
  end

  (1..number_of_workers).each do |node_number|
    config.vm.define "worker#{node_number}" do |worker|
      worker.vm.hostname = "worker#{node_number}"
      ip = node_number + 100
      worker.vm.network :private_network,
        :ip => "192.168.80.#{ip}",
        :netmask => "255.255.255.0"
      worker.vm.synced_folder './', '/vagrant', type: 'nfs'
      worker.vm.provision :shell, :path => "worker.sh"
      worker.vm.provider :libvirt do |vm|
        vm.driver = 'kvm'
        vm.cpu_mode = 'host-passthrough'
        vm.cpus=1
        vm.memory=2048
      end
    end
  end
end
