#/bin/bash
set -ex

export WORKER_IP=$(ip a |grep global | grep eth1 | awk '{print $2}' | cut -f1 -d '/')
export MASTER_IP=$(cat /vagrant/server-ip)
export NODE_TOKEN=$(cat /vagrant/node-token)

yum install -y container-selinux selinux-policy-base
rpm -i https://rpm.rancher.io/k3s-selinux-0.1.1-rc1.el7.noarch.rpm

# disabled as mongo doesn't like this
echo never >/host-sys/kernel/mm/transparent_hugepage/enabled

curl -sfL https://get.k3s.io | \
  K3S_URL="https://${MASTER_IP}:6443" \
  K3S_TOKEN="${NODE_TOKEN}" \
  INSTALL_K3S_EXEC="\
    --node-ip=${WORKER_IP} \
    --node-external-ip=${WORKER_IP}" \
    sh -
